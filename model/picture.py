class Picture:

    def __init__(self, id, title, description, s3_path=None) -> None:
        self.__id = id
        self.title = title
        self.s3_path = s3_path
        self.description = description
        self.children = ()

    @property
    def id(self):
        return self.__id

    def __str__(self) -> str:
        return "'{}': '{}' located in S3: '{}' with id '{}'".format(
            self.title, self.description, self.s3_path, self.__id
        )

    def __repr__(self) -> str:
        return "model.Picture({}, {})".format(
            self.title, self.description
        )

