class Parent:
    def __init__(self, id, first_name, last_name, email, cell_phone) -> None:
        self.__id = id
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.cell_phone = cell_phone
        self.children = ()

    @property
    def id(self):
        return self.__id

    def __repr__(self) -> str:
        return "parent.Parent({}, '{}', '{}', '{}', '{}')".format(
            self.__id, self.first_name, self.last_name, self.email, self.cell_phone
        )

    def __str__(self) -> str:
        return "#{}: {} {}, email: {}, cell phone: {}" \
            .format(self.__id, self.first_name, self.last_name, self.email, self.cell_phone)
