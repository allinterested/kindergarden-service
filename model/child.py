class Child:
    def __init__(self, id, first_name: str, last_name: str, birth_date: str) -> None:
        self.__id = id
        self.first_name = first_name
        self.last_name = last_name
        self.birth_date = birth_date
        self.parent_ids = ()
        self.pictures = []

    @property
    def id(self):
        return self.__id

    def __str__(self) -> str:
        return "#{}: {} {}, {}".format(
            self.__id, self.first_name, self.last_name, self.birth_date
        )

    def __repr__(self) -> str:
        return "child.Child({}, '{}', '{}', '{}')".format(
            self.__id, self.first_name, self.last_name, self.birth_date
        )

