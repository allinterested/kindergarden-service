from model.child import Child
from pyOptional import Optional

children = {
    "1": Child(1, 'John', 'Fox', '12-04-2018'),
    "2": Child(2, 'Jane', 'Fox', '15-05-2019'),
}

children["1"].parent_ids = (1, 2)
children["2"].parent_ids = (1, 2)


class ChildDao:
    def get_child(self, child_id) -> Optional(Child):
        try:
            return Optional(children[str(child_id)])
        except KeyError:
            return Optional.empty()

    def add_child(self, child):
        if str(child.id) not in children.keys():
            children[str(child.id)] = child
        return child.id

    def get_children(self):
        return list([item[1] for item in children.items()])
