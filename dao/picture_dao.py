import uuid

from pyOptional import Optional
from model.picture import Picture

storage = {
    "1": Picture(1, "Summer vibes", "Very good picture",
                 "https://my-bucket.s3.us-west-2.amazonaws.com/pictures/{}".format(str(uuid.uuid4()))),
    "2": Picture(2, "Indian trip", "It was very funny here!",
                 "https://my-bucket.s3.us-west-2.amazonaws.com/pictures/{}".format(str(uuid.uuid4()))),
    "3": Picture(3, "Happy New Year", "All the best in upcoming year",
                 "https://my-bucket.s3.us-west-2.amazonaws.com/pictures/{}".format(str(uuid.uuid4())))
}

storage["1"].children = (1, 2)


class PictureDao:

    def save_picture(self, picture) -> Picture:
        storage[str(picture.id)] = picture
        return picture

    def get_picture(self, picture_id) -> Optional(Picture):
        try:
            return Optional(storage[str(picture_id)])
        except KeyError:
            return Optional.empty()
