from pyOptional import Optional

from model.parent import Parent

parents = {
    "1": Parent(1, 'Alex', 'Fox', 'alexfox@test.com', '555-666-345'),
    "2": Parent(2, 'Jill', 'Fox', 'jillfox@test.com', '555-666-346'),
    "3": Parent(3, 'Bill', 'Jackson', 'billjackson@test.com', '555-666-346'),
}

parents["1"].children = (1, 2)


class ParentDao:
    def get_parent(self, parent_id) -> Optional(Parent):
        try:
            return Optional(parents[str(parent_id)])
        except KeyError:
            return Optional.empty()

    def get_all_parents(self):
        return list([item[1] for item in parents.items()])

