import unittest

from dao.parent_dao import ParentDao
from dto.children_for_parent_dto import ChildrenForParentDto
from exceptions.entity_not_found_exception import EntityNotFoundException
from model.child import Child
from model.parent import Parent
from service.parent_service import ParentService


class TestParentService(unittest.TestCase):
    def setUp(self) -> None:
        self.parent_service = ParentService(ParentDao())
        self.parent_alex = Parent(1, 'Alex', 'Fox', 'alexfox@test.com', '555-666-345')
        self.parent_jill = Parent(2, 'Jill', 'Fox', 'jillfox@test.com', '555-666-346')
        self.parent_bill = Parent(3, 'Bill', 'Jackson', 'billjackson@test.com', '555-666-346')
        self.child_john = Child(1, 'John', 'Fox', '12-04-2018')
        self.child_jane = Child(2, 'Jane', 'Fox', '15-05-2019')

    def test_should_fetch_all_parents(self):
        parents = self.parent_service.get_all_parents()
        self.assertTrue(len(parents) > 0)
    def test_parent_has_no_children_after_creation(self):
        self.assertTupleEqual((), self.parent_alex.children)

    def test_should_add_child_to_parent(self):
        self.parent_service.add_child_to_parent(self.parent_alex, [self.child_john])
        print(self.parent_alex.__str__())
        self.assertEqual(len(self.parent_alex.children), 1)

    def test_should_add_two_children_to_parent(self):
        self.parent_service.add_child_to_parent(self.parent_alex, [self.child_john, self.child_jane])
        self.assertEqual(len(self.parent_alex.children), 2)

    def test_cannot_add_more_than_two_parents_for_one_child(self):
        with self.assertRaises(AttributeError):
            self.parent_service.add_child_to_parent(self.parent_alex, [self.child_john])
            self.parent_service.add_child_to_parent(self.parent_jill, [self.child_john])
            self.parent_service.add_child_to_parent(self.parent_bill, [self.child_john])

    def test_should_get_parent_with_id(self):
        parent_id = 2
        existing_parent = self.parent_service.get_parent(parent_id)
        expected_parent_name = 'Jill'
        self.assertEqual(expected_parent_name, existing_parent.first_name)

    def test_should_get_none_for_invalid_parent_id(self):
        invalid_id = 666
        parent = self.parent_service.get_parent(invalid_id)
        self.assertEqual({}, parent)

    def test_should_get_children_for_valid_parent(self):
        expected_child_name = 'John'
        parent_id = 1
        resp: ChildrenForParentDto = self.parent_service.get_children_for_parent(parent_id)
        print("\n {}".format(resp.children[0]))
        self.assertEqual(expected_child_name, resp.children[0].first_name)

    def test_should_fail_when_get_children_for_non_existing_parent(self):
        invalid_id = 666
        resp = self.parent_service.get_children_for_parent(invalid_id)
        self.assertEqual({}, resp.children)
