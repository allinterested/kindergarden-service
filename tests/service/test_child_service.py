from unittest import TestCase

from dao import child_dao
from dao.child_dao import ChildDao
from exceptions.entity_not_found_exception import EntityNotFoundException
from model.child import Child
from service.child_service import ChildService


class TestChildService(TestCase):
    def setUp(self) -> None:
        self.child_service = ChildService(ChildDao())

    def test_should_get_child_with_id(self):
        child_id = 1
        child_name = 'John'
        child = self.child_service.get_child(child_id)
        self.assertEqual(child_name, child.first_name)

    def test_should_error_for_fetching_child_with_invalid_id(self):
        invalid_id = 666
        with self.assertRaises(EntityNotFoundException):
            self.child_service.get_child(invalid_id)

    def test_should_add_child_to_db(self):
        sam = Child(4, 'Sam', 'Lapkison', '23-04-2016')
        child_id = self.child_service.add_child(sam)
        self.assertEqual(sam.id, child_id)

    def test_cannot_add_child_with_duplicate_id(self):
        child_id = 1
        john = self.child_service.get_child(child_id)
        june = Child(child_id, 'June', 'Smith', '12-06-1990')
        self.child_service.add_child(june)
        self.assertEqual(john.first_name, self.child_service.get_child(child_id).first_name)