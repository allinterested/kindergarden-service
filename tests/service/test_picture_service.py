from unittest import TestCase

from dao.child_dao import ChildDao
from dao.picture_dao import PictureDao
from external.s3_storage_client import S3StorageClient
from model.child import Child
from model.picture import Picture
from service.child_service import ChildService
from service.picture_service import PictureService


class TestPictureService(TestCase):
    def setUp(self) -> None:
        self.picture = Picture(34, 'Summer vibes', 'Getting all together in a cafe!')
        self.service = PictureService(PictureDao(), S3StorageClient(), ChildService(ChildDao()))
        self.child = Child(56, 'Alex', 'Kotofski', '12-05-2016')

    def test_should_add_picture(self):
        resp = self.service.add_picture(self.picture)
        self.assertIsNotNone(resp.s3_path)

    def test_get_picture_by_id(self):
        picture_id = 3
        expected_title = "Happy New Year"
        resp = self.service.get_picture(picture_id)
        self.assertEqual(expected_title, resp.title)

    def test_should_add_children_to_the_picture(self):
        self.service.mark_children_on_picture(self.picture, self.child)
        self.assertEqual(self.child.id, self.picture.children[0])

    def test_should_return_picture_with_children(self):
        picture_id = 1
        child_name = 'John'
        resp = self.service.get_picture_with_children(picture_id)
        self.assertEqual(child_name, resp.children[0].first_name)

    def test_should_return_only_picture_when_no_children_marked_on_it(self):
        picture_id = 2
        resp = self.service.get_picture_with_children(picture_id)
        self.assertEqual(0, len(resp.children))

    def tearDown(self) -> None:
        pass
