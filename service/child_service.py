from dao.child_dao import ChildDao
from exceptions.entity_not_found_exception import EntityNotFoundException
from model.child import Child
import logging

logging.basicConfig(level=logging.INFO,format='%(asctime)s:%(levelname)s:%(module)s:%(message)s')

class ChildService:

    def __init__(self, child_dao: ChildDao) -> None:
        super().__init__()
        self.child_dao = child_dao

    def get_child(self, child_id) -> Child:
        logging.info("Fetching child for id {}".format(child_id))
        msg = "No child found for id {}".format(child_id)
        child = self.child_dao.get_child(child_id).get_or_raise(EntityNotFoundException, msg)
        logging.info("Child fetched: {}".format(child.__str__()))
        return child

    def add_child(self, child):
        logging.info("Adding child {}".format(child.__str__()))
        return self.child_dao.add_child(child)

    def get_children(self):
        logging.info("Fetching children")
        return self.child_dao.get_children()
