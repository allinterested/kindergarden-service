from dao.child_dao import ChildDao
from dao.parent_dao import ParentDao
from dto.children_for_parent_dto import ChildrenForParentDto
from exceptions.entity_not_found_exception import EntityNotFoundException
from model.parent import Parent
from model.child import Child
from service.child_service import ChildService
import logging

logging.basicConfig(level=logging.INFO,format='%(asctime)s:%(levelname)s:%(module)s:%(message)s')

class ParentService:

    def __init__(self, parent_dao: ParentDao) -> None:
        super().__init__()
        self.parent_dao = parent_dao
        self.child_service = ChildService(ChildDao())

    def add_child_to_parent(self, parent: Parent, children: list) -> Parent:
        logging.info("Adding children to parent '{}' \n".format(parent.__str__()))
        [self.__parent_to_child(parent, ch) for ch in children]
        parent.children = parent.children + tuple(ch.id for ch in children)
        return parent

    def __parent_to_child(self, parent: Parent, child: Child):
        if len(child.parent_ids) < 2:
            child.parent_ids = child.parent_ids + (parent.id,)
        else:
            logging.error("Can not assign more than 2 parents to the child")
            raise AttributeError("Can not assign more than 2 parents to the child")

    def get_children_for_parent(self, parent_id: int) -> ChildrenForParentDto:
        parent = self.get_parent(parent_id)
        children = {}
        if parent != {}:
            logging.info("Fetching children for parent '{}' \n".format(parent.__str__()))
            children = tuple(self.child_service.get_child(childId) for childId in parent.children)
            logging.info("Children: {} \n".format([ch.__str__() for ch in children]))
        return ChildrenForParentDto(parent, children)

    def get_parent(self, parent_id) -> Parent:
        logging.info("Fetching parent for id {}".format(parent_id))
        parent = self.parent_dao.get_parent(parent_id).get_or_else({})
        logging.info("Parent fetched: {}".format(parent.__str__()))
        return parent

    def get_all_parents(self):
        logging.info("Fetching all parents")
        return self.parent_dao.get_all_parents()
