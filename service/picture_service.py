import logging

from dto.picture_with_children_dto import PictureWithChildrenDto

logging.basicConfig(level=logging.INFO,format='%(asctime)s:%(levelname)s:%(module)s:%(message)s')
class PictureService:

    def __init__(self, dao, storage_client, child_service) -> None:
        super().__init__()
        self.dao = dao
        self.storage_client = storage_client
        self.child_service = child_service

    def add_picture(self, picture):
        logging.info("Saving picture: {}".format(picture.__str__()))
        s3_path = self.storage_client.store(picture) \
            .get_or_raise(Exception, "Fail to store picture in external storage")
        picture.s3_path = s3_path
        self.dao.save_picture(picture)
        logging.info("Picture {} saved.".format(picture.__str__()))
        return picture

    def get_picture(self, picture_id):
        pict = self.dao.get_picture(picture_id)\
            .get_or_raise(Exception, "Picture with id '{}' not found".format(picture_id))
        logging.info("Fetched picture: {}".format(pict.__str__()))
        return pict

    def mark_children_on_picture(self, picture, child):
        logging.info("Adding child '{}' to picture '{}'"
                     .format(child.__str__(), picture.__str__()))
        picture.children = picture.children + (child.id, )

    def get_picture_with_children(self, picture_id):
        logging.info("Fetching children for picture id '{}'".format(picture_id))
        picture = self.get_picture(picture_id)
        children = []
        if len(picture.children) > 0:
            children = [self.child_service.get_child(id) for id in picture.children]
        return PictureWithChildrenDto(picture, children)
