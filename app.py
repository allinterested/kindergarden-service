import json

from flask import Flask

from dao.child_dao import ChildDao
from dao.parent_dao import ParentDao
from schema.child_schema import ChildSchema
from schema.parent_schema import ParentSchema
from service.child_service import ChildService
from service.parent_service import ParentService

app = Flask(__name__)

parent_service = ParentService(ParentDao())
child_service = ChildService(ChildDao())

parents_schema = ParentSchema(many=True)
parent_schema = ParentSchema()
children_schema = ChildSchema(many=True)
child_schema = ChildSchema()


@app.get("/parents")
def get_parents():
    parents = parent_service.get_all_parents()
    return parents_schema.dump(parents)


@app.get("/parent/<int:parent_id>")
def get_parent(parent_id):
    parent = parent_service.get_parent(parent_id)
    return parent_schema.dump(parent)


@app.get("/child")
def get_children():
    children = child_service.get_children()
    return children_schema.dump(children)


@app.get("/child/<int:child_id>")
def get_child(child_id):
    return child_schema.dump(child_service.get_child(child_id))


if __name__ == '__main__':
    app.run()
