import logging
import uuid

from pyOptional import Optional

logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(module)s:%(message)s')


class S3StorageClient:

    def store(self, picture):
        s3_location = "https://my-bucket.s3.us-west-2.amazonaws.com/pictures/{}".format(str(uuid.uuid4()))
        logging.info("Picture S3 location: {}".format(s3_location))
        return Optional(s3_location)
