from marshmallow import Schema, fields


class ParentSchema(Schema):
    id = fields.Int()
    first_name = fields.Str()
    last_name = fields.Str()
    email = fields.Email()
    cell_phone = fields.Str()
    children = fields.Tuple((fields.Int(),fields.Int(),fields.Int()))
