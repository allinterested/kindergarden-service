from marshmallow import Schema, fields


class ChildSchema(Schema):
    id = fields.Int()
    first_name = fields.Str()
    last_name = fields.Str()
    birth_date = fields.Str()
    parent_ids = fields.Tuple((fields.Int(), fields.Int()))
    pictures = fields.List(fields.Int())
