from model.parent import Parent


class ChildrenForParentDto:
    def __init__(self, parent: Parent, children) -> None:
        super().__init__()
        self.parent = parent
        self.children = children
