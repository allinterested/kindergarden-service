class PictureWithChildrenDto():

    def __init__(self, picture, children) -> None:
        super().__init__()
        self.__picture = picture
        self.__children = children

    @property
    def picture(self):
        return self.__picture

    @property
    def children(self):
        return self.__children